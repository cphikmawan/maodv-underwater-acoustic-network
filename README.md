# MAODV Underwater Acoustic Network

#### Outline
##### [1. Konsep Modifikasi](#1-konsep-modifikasi)
##### [2. Modifikasi Fungsi](#2-modifikasi-fungsi)

#### Assets

[1. Source Code](aodv)
[2. Setdes, CBR, debug.txt](param)

#### Profil :
|||
| --- | --- |
| Nama | Cahya Putra Hikmawan |
| NRP | 05111540000119 |
| Kelas | Jaringan Nirkabel |

#### Bahan
| Bahan | Keterangan |
| --- | --- |
| Paper MAODV | [Underwater Acoustic Network](https://ieeexplore.ieee.org/document/7879681) |
| Linux OS | 18.04 Bionic |
| NS2 | Network Simulator 2 |
| GCC | Versi 4.8 |
| G++ | Versi 4.8 |

#### Konsep Modifikasi
Pada studi kasus di paper yang akan dibahas ini adalah tentang jaringan akustik bawah air, dikarenakan medan yang ekstrem mengakibatkan paket RREQ sering tidak ditransmisikan dan diterima, jika sampai batas waktu tertentu hal tersebut bisa mengakibatkan kemacetan dan penundaan transmisi paket dan dapat beresiko adanya kelumpuhan pada jaringan tersebut. Dikarenakan adanya pengaturan rute terhadap rute pada AODV biasa, terkadang rute yang tidak memiliki kegagalan sebelumnya pun akan dihapus, oleh karena itu pada paper ini akan menawarkan beberapa modifikasi untuk menanggulangi beberapa kasus-kasus di atas:

1. Doubling Flooding pada Paket Header
2. Flag Route Error pada Routing Table

#### Modifikasi
1. File **_aodv_packet.h_**

Untuk menambahkan variabel untuk flag di paket header

2. File **_aodv_rtable.h_**

Untuk menambahkan variabel pada saat memodifikasi routing table

3. File **_aodv.h_**

Untuk menambahkan parameter fungsi yang akan dimodifikasi dan digunakan

4. File aodv.cc

Fungsi yang diubah :

- sendRequest, sendError, recvRequest, rt_update.


#### Referensi
- https://ieeexplore.ieee.org/document/7879681
- https://arxiv.org/pdf/1007.4065.pdf
- http://www.nsnam.com/2018/06/installation-of-ns2-in-ubuntu-1804.html
- http://intip.in/RA2018